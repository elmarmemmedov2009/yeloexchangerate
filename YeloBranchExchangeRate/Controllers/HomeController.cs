﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using YeloBranchExchangeRate.Models;

namespace YeloBranchExchangeRate.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {

            var url = ConfigurationManager.AppSettings["branchesListUrl"];
            CustomBranches branches = new CustomBranches();
            try
            {
                string jsonData = Utils.CreateGetRequest(url);
                branches = new JavaScriptSerializer().Deserialize<CustomBranches>(jsonData);
                Session["Branches"] = branches;
            }
            catch (Exception)
            {
                ViewBag.Errors = "Xəta baş verdi";
            }

            return View(branches);
        }

        [HttpPost]
        public ActionResult Index(SettingsModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Errors = ModelState.Values.SelectMany(v => v.Errors);
                return View();
            }
            int timeInMillis = 0;
            if (model.TimeType == "min")
            {
                timeInMillis = model.TimeValue * 60 * 1000;
            }
            else if (model.TimeType == "hour")
            {
                timeInMillis = model.TimeValue * 60 * 60 * 1000;
            }

            model.TimeInMillis = timeInMillis;

            return View("MainExchange", model);
        }


        public JsonResult GetRates(string branchCode)
        {
            BranchData branchData = new BranchData();
            try
            {
                var url = ConfigurationManager.AppSettings["exchangeRateUrl"];
                string jsonData = Utils.CreatePostRequest(url, new BranchModel { Branch = branchCode });
                branchData = new JavaScriptSerializer().Deserialize<BranchData>(jsonData);
            }
            catch (Exception e)
            {
                int? sentErrorCount = Session["ErrorSentCount"] as int?;
                if (sentErrorCount == null)
                {
                    Session["ErrorSentCount"] = 0;
                    sentErrorCount = 0;
                }

                if (sentErrorCount % 10 == 0)
                {
                    Utils.SendErrorMessage(branchCode);
                }
                Session["ErrorSentCount"] = ++sentErrorCount;

                Json(new { result = false, html = "" });
            }
            string viewHtml = PartialView("GetRates", branchData).RenderToString();
            return Json(new { result = true, html = viewHtml }, JsonRequestBehavior.AllowGet);
        }

    }
}