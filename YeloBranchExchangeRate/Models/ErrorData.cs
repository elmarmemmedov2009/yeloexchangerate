﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YeloBranchExchangeRate.Models
{
    public class ErrorData
    {
        public bool HasError { get; set; }
        public DateTime ErroDateTime { get; set; }
    }
}