﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YeloBranchExchangeRate.Models
{
    public class Branch
    {
        public string code { get; set; }
        public string name { get; set; }
    }
}