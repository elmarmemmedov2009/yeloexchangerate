﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YeloBranchExchangeRate.Models
{
    public class BranchData
    {
        public List<Rate> Rates { get; set; }

        public BranchData()
        {
            Rates = new List<Rate>();
        }
    }
}