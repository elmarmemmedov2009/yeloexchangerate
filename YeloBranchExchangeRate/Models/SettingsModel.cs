﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YeloBranchExchangeRate.Models
{
    public class SettingsModel
    {
        [Required]
        public int BranchCode { get; set; }

        [Required]
        public int TimeValue { get; set; }

        [Required]
        public string TimeType { get; set; }

        public int TimeInMillis { get; set; }
    }
}