﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YeloBranchExchangeRate.Models
{
    public class CustomBranches
    {
        public List<Branch> Branches { get; set; }
    }
}