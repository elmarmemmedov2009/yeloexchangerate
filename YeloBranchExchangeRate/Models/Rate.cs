﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YeloBranchExchangeRate.Models
{
    public class Rate
    {
        public string buy;
        public string sell;



        public string Name { get; set; }
        public string Buy
        {
            get { return buy; }
            set
            {

                buy = string.Format("{0:f4}", Math.Round(decimal.Parse(value), 4));
            }
        }
        public string Sell
        {
            get
            {
                return sell;
            }
            set
            {
                sell = string.Format("{0:f4}", Math.Round(decimal.Parse(value), 4));
            }
        }
        public string Icon { get; set; }
    }
}