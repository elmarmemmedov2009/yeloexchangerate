﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace YeloBranchExchangeRate.Models
{
    public class Utils
    {

        public static string SendSms(string phoneNumber, string text)
        {
            string url = $"http://api.msm.az/sendsms?user=nikoilsmppapi&password=SzBdVsUB&gsm={phoneNumber}&from=Yelo%20Bank&text={text}";
            string result = CreateGetRequest(url);
            return result;
        }



        public static string CreateGetRequest(string url)
        {
            string jsonData = null;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                jsonData = reader.ReadToEnd();
            }

            return jsonData;

        }



        public static string CreatePostRequest<T>(string url, T t)
        {

            string jsonData = null;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
                MemoryStream ms = new MemoryStream();
                ser.WriteObject(ms, t);
                string jsonString = Encoding.UTF8.GetString(ms.ToArray());
                ms.Close();
                streamWriter.Write(jsonString);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                jsonData = result;
            }

            return jsonData;
        }





        public static string GetBranchNameByCode(string branchCode)
        {
            string branchName = null;
            CustomBranches branches = HttpContext.Current.Session["Branches"] as CustomBranches;

            if (branches != null)
            {
                foreach (Branch branch in branches.Branches)
                {
                    if (branch.code == branchCode)
                    {
                        branchName = branch.name;
                        break;
                    }
                }
            }

            return branchName;

        }

        internal static void SendErrorMessage(string branchCode)
        {
            string branchName = GetBranchNameByCode(branchCode);
            List<string> phoneNumbers = GetPhoneNumbers();

            foreach (string phoneNumber in phoneNumbers)
            {
                SendSms(phoneNumber, $"{branchName} filialda mezenne gosteren zaman xeta baş verdi. BranchCode: {branchCode}");
            }

        }

        internal static List<string> GetPhoneNumbers()
        {
            var phoneNumber = ConfigurationManager.AppSettings["phoneNumberForErrorMessage"];
            List<string> phoneNumbers = phoneNumber != null && phoneNumber.Split('|')?.Length > 0 ? phoneNumber.Split('|').ToList() : new List<string>();

            return phoneNumbers;
        }
    }

}